// variable
const playerBatu = document.getElementById("click-b");
const playerGunting = document.getElementById("click-g");
const playerKertas = document.getElementById("click-k");
const comBatu = document.getElementById("clickcom-b");
const comGunting = document.getElementById("clickcom-g");
const comKertas = document.getElementById("clickcom-k");
const refresh = document.getElementById("refresh");
const winBox = document.getElementById("versus-box");
const saDel = document.getElementById("h1");
const x = document.querySelector("player-tools");
const button = document.querySelector(`button`);
const addEl = [document.getElementsByClassName="app"];

// computer random alghorythm

function comRandom() {
  var choices = [`batu`, `gunting`, `kertas`];
  var randomAlgh = Math.floor(Math.random() * 3);
  return choices[randomAlgh];
};

// Win - Lose - Draw Hasil
function win(){
  console.log("P 1 Win");
  hasilObject();
  saDel.innerText = "P 1 Win";
}

function lose(){
  console.log("Com Win");
  hasilObject();
  saDel.innerText = "Com Win"
}

function draw(){
  console.log("Draw Dong");
  hasilObject();
  saDel.innerText = "Draw Dong"
}

// COndition
function gameCondition(Pemain) {
  const computerPlayer = comRandom();
  console.log("hasil BGK =>" + Pemain);
  console.log("hasil BGK =>" + computerPlayer);

  switch (Pemain + computerPlayer) {
    case "KertasBatu":
    case "GuntingKertas":
    case "BatuGunting":
      win();

      break;
    case "BatuKertas":
    case "GuntingBatu":
    case "KertasGunting":
      lose();

      break;
    case "BatuBatu":
    case "GuntingGunting":
    case "KertasKertas":
      draw();
  }

  switch (computerPlayer) {
    case "batu":
      comBatu.classList.add(`chosen`);

    case "gunting":
      comGunting.classList.add(`chosen`);
    
    case "kertas":
      comKertas.classList.add(`chosen`);
  }
}

// Hasil warna
function resultObject() {
  winBox.classList.add('winBox'),
   saDel.setAttribute("style", "font-size:30px; color:pink;");

}
function resultDraw() {
  winBox.classList.add('drawBox');
   saDel.setAttribute("style", "font-size:30px; color:pink;"); 

// Pemain
function play() {
  playerBatu.addEventListener(`click`, function(){
    this.classList.add(`chosen`);
    gameCondition("Batu");
    addEl.forEach(addEl3=> {
      addEl3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
  })
  playerGunting.addEventListener(`click`, function(){
    this.classList.add(`chosen`);
    gameCondition("Gunting");
    addEl.forEach(addEl3=> {
      addEl3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
  })
  playerKertas.addEventListener(`click`, function(){
    this.classList.add(`chosen`);
    gameCondition("Kertas");
    addEl.forEach(addEl3=> {
      addEl3.setAttribute("style", "cursor: not-allowed;pointer-events: none;")
    })
  })
}
  play();

  // Refresh

  refresh.addEventListener(`click`, function () {

    addEl.forEach(addEl3 => {
      addEl3.classList.remove(`chosen`)
    });
    addEl.forEach(addEl2 => {
      addEl2.classList.remove(`chosen`)
    });
  
    winBox.classList.remove(`Winbox`);
    drawBox.classList.remove(`Drawbox`);
    saDel.removeAttribute(`style`, "color; ``; font-size:``")
  
    saDel.style.marginTop = null
    saDel.style.fontSize = null
    saDel.innerText = null
    button.disabled = false;
  })
}
