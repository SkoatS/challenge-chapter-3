const express = require('express');
const app = express();
const path = require('path');


app.set("view engine", "ejs");

app.use("/image", express.static(path.join(__dirname, "/image")));
app.use(express.static(__dirname));


app.get("/", function(req, res){
    res.render(__dirname + "/gameBGK.ejs")
});

app.listen(8011, function() {
    console.log("App running at http://localhost:8011")
});